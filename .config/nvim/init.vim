call plug#begin()
Plug 'ptzz/lf.vim'
call plug#end()

set clipboard=unnamedplus
set number
set autoindent

vmap <C-y> "+y
vmap <C-p> "+p
imap <C-p> <ESC>"+p
nmap <C-p> "+p
nmap <C-a> ggVG
